<?php
if (isset($_GET['submit'])) {

    $nama = $_GET['nama'];
    $mapel = $_GET['mapel'];
    $uts = $_GET['uts'];
    $uas = $_GET['uas'];
    $tugas = $_GET['tugas'];

    $nilai_uts = $uts * 0.35;
    $nilai_uas = $uas * 0.5;
    $nilai_tugas = $tugas * 0.15;

    $nilai_total = $nilai_uts + $nilai_uas + $nilai_tugas;

    if ($nilai_total >= 90 && $nilai_total <= 100) {
        $grade = "A";
    } elseif ($nilai_total > 70 && $nilai_total < 90) {
        $grade = "B";
    } elseif ($nilai_total > 50 && $nilai_total <= 70) {
        $grade = "C";
    } elseif ($nilai_total <= 50) {
        $grade = "D";
    }
}
?>

<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" 
    rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">

    <title>Form Penilaian</title>
</head>

<body>
    <div class="h1">
        <center>FORM PENILAIAN SISWA</center>
    </div>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-6 border border-primary mt-3 p-3">
                <form>
                    <div class="mb-3">
                        <label for="exampleInputNama" class="form-label">NAMA SISWA/SISWI</label>
                        <input type="text" name="nama" required ="" class="form-control" id="exampleInputNama">
                    </div>
                    <div class="mb-3">
                        <label for="exampleInputMapel" class="form-label">MATA PELAJARAN</label>
                        <input type="text" name="mapel" required ="" class="form-control" id="exampleInputMapel">
                    </div>
                    <div class="mb-3">
                        <label for="exampleInputNilaiUts" class="form-label">NILAI UTS</label>
                        <input type="number" min="0" max="100"name="uts" required ="" class="form-control" id="exampleInputNilaiUts">
                    </div>
                    <div class="mb-3">
                        <label for="exampleInputNilaiUas" class="form-label">NILAI UAS</label>
                        <input type="number" min="0" max="100" name="uas" required ="!"class="form-control" id="exampleInputNilaiUas">
                    </div>
                    <div class="mb-3">
                        <label for="exampleInputNilaiTugas" class="form-label">NILAI TUGAS</label>
                        <input type="number" min="0" max="100" name="tugas" required ="" class="form-control" id="exampleInputNilaiTugas">
                    </div>
                    <button type="kirim" name="submit" class="btn btn-primary">KIRIM</button>
                </form>
            </div>
        </div>
        <?php if(isset($_GET['submit'])): ?>
        <div class="row justify-content-center">
            <div class="col-6 border border-primary mt-3 p-3">
                <div class="alert alert-success">
                    NAMA LENGKAP    : <?php echo $nama ?> <br>
                    MATA PELAJARAN  : <?php echo $mapel ?> <br>
                    NILAI TUGAS     : <?php echo $tugas ?> <br>
                    NILAI UTS       : <?php echo $uts ?> <br>
                    NILAI UAS       : <?php echo $uas ?> <br>
                    NILAI TOTAL     : <?php echo $nilai_total ?> <br>
                    GRADE           : <?php echo $grade ?> <br>
                </div>
            </div>
        </div>
        <?php endif; ?>
    </div>
        <!-- Optional JavaScript; choose one of the two! -->

        <!-- Option 1: Bootstrap Bundle with Popper -->
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-gtEjrD/SeCtmISkJkNUaaKMoLD0//ElJ19smozuHV6z3Iehds+3Ulb9Bn9Plx0x4" crossorigin="anonymous"></script>
</body>
</html>